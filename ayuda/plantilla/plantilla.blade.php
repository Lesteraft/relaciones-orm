@extends('layouts.app')

@section('style')
    <style>
        main {
            background-color: #f8fafc !important
        } 
        .alert{
            padding: 0.35rem 0.85rem 
        }
        .ui.modal {
            position: initial !important
        }
        .content-post {
            font-size: 1.2em
        }
    </style>
    <link rel="stylesheet" href="css/semantic.css">
    <link rel="stylesheet" href="css/icon.css">
    {{-- <link rel="stylesheet" href="css/modal.css"> --}}
@endsection
@section('content')
<?php require_once('php/timeAgo.php');?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Posts</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="ui feed">


                        @foreach ($posts as $post)
                        <div class="card mt-5">
                            <div class="ui feed mb-0 ml-5 mr-5 mt-5" >
                                <div class="event">
                                    <div class="label">
                                        <img src="{{$post->user->image}}">
                                    </div>
                                    <div class="content content-post">
                                        <div class="summary">
                                        <a class="author"  onclick="modal('{{$post->user->image}}', '{{$post->user->name}}', '{{$post->user->email}}', {{$post->user->roles}}, {{$post->user->phone->number}} )">{{ $post->user->name }}</a> 
                                            <div class="date">
                                                posted on his page
                                            </div>
                                        </div>
                                        <div class="text">
                                            {{$post->post}}
                                        </div>
                                        <div class="meta w-100 text-right">
                                            <small class="like">
                                                {{date("F j, Y, g:i a", strtotime($post->created_at))}}
                                            </small>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body pt-0 ml-5 mr-5">
                                <h3 class="ui dividing header">Comments</h3>
                                <div class="ui minimal comments m-auto">
                                    @foreach ($post->comments as $comment)
                                    <div class="comment">
                                        <a class="avatar">
                                            <img src="{{$comment->user->image}}">
                                        </a>
                                        <div class="content alert alert-secondary w-auto d-table">
                                          <a class="author"  onclick="modal('{{$comment->user->image}}', '{{$comment->user->name}}', '{{$comment->user->email}}',   {{$comment->user->roles}}, {{$comment->user->phone->number}} )" >{{$comment->user->name}}</a>
                                          <div class="metadata">
                                            <span class="date">{{get_time_ago(strtotime($comment->created_at))}}</span>
                                          </div>
                                          <div class="text">
                                            {{$comment->comment}}
                                          </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    <form class="ui reply form">
                                      <div class="field">
                                        <textarea class="w-100 h-100" ></textarea>
                                      </div>
                                      <div class="btn btn-primary">Add Reply
                                      </div>
                                    </form>
                                  </div>
                            </div>
                        </div>
                        @endforeach


                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- MODAL --}}
  
     <!-- Modal -->
     <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header header">
              <h5 class="modal-title" id="exampleModalCenterTitle">Profile</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body ui modal d-block w-100 shadow-none">
                <div class="image content">
                    <div class="ui small image">
                      <img id="userImage" src="https://b.thumbs.redditmedia.com/bklLsi-PwlfkOjcHguSxnT-AQ_6McHbSzquxax0guAM.png" >
                    </div>
                    <div class="description">
                      <div class="ui header" id="userName">Juanito Alcachofa</div>
                      <p id="userEmail">juanito@gmail.com</p>
                      <p>Roles: <small class="ml-3" id="userRole">Admin</small></p>
                    </div>
                </div>
            </div>
            <div class="modal-footer actions">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection

@section('script')
<Script>

    function modal(image, nombre, correo, roles ) {
        console.log(roles);
        
        $("#userName").text(nombre);
        $("#userEmail").text(correo);
        $("#userRole").html('');
        roles.forEach(role => {
            $("#userRole").append(`<div class="ui basic label">${role.name}</div>`)
        });;
        // $("#userImage").attr('src', image);
        $('#exampleModalCenter').modal('show');
    }
</Script>
<script src="js/semantic.js"></script>

@endsection