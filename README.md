# ¿Cómo ejecuto este proyecto?
1. Ejecuta el comando "composer install" para las dependencias de composer
2. Ejecuta el comando "npm install" para las dependencias de NodeJS
3. Ejecuta el comando "npm run dev"
4. Copia y pega lo que está en el archivo .env.example en un nuevo archivo llamado .env


## Relaciones en Eloquent ORM

Este es un pequeño ejemplo en el cual se pretende mostrar de manera sencilla el funcionamiento de 4 relaciones:

-   One To One
-   One To Many
-   One To Many (Inverse)
-   Many To Many

Para ello se provee de ejemplos usando la herramienta tinker, ejecutando el comando

```
php artisan tinker.
```

## One To One

-   Se utiliza en relaciones 1:1 , por ejemplo la relacion entre un usuario y un telefono.

```
    App\User::find(1)->phone;
```

-   Por el lado contrario

```
    App\Phone::find(1)->user;
```

## One To Many

Se utiliza en relaciones uno a muchos por ejemplo, un post tiene varios comentarios.

- Para acceder a la información seguimos los comandos:

```
    $post = Post::where('post_id',1)->first()

    foreach ($comments as $comment) {
        echo $comment->comment;
    }
```

## One To Many (Inverse)

Es la forma inversa de la relacion uno a muchos es decir de muchos a uno, como tenemos que varios comentarios pertenecen a un post.

- Para acceder a la información seguimos los comandos:

```
    $comment = Comment::where('commet_id',1)->first();
```

## Many To Many

Se usa como ejemplo las tablas roles y usuarios en donde estos forman una relacion n:m
- Para obtener los roles de un usuario usamos los comandos:
    ```
    $user = User::find(1);

    dd($user->roles);
    ```

- Para obtener los usuarios de un rol:
    ```
    $role = Role::find(1);

    dd($role->users);
    ```

- Para crear registros en la tabla intermedia:
    ```
    $user = User::find(3);

    $roleIds = [1, 2];
    $user->roles()->sync($roleIds);

    $role = Role::find(2);	
 
    $userIds = [3];
    $role->users()->sync($userIds);
    ```

- Para acceder a las columna created_at de la tabla intermedia role_user:
    ```
    $user = App\User::find(1);

    foreach ($user->roles as $role) {
        echo $role->role_user->created_at;
    }
    ```


