<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
        protected $primaryKey = 'user_id';
        public $incrementing = false; 
        protected $fillable = [
                'user_id',
                'number'
        ];

        public function user()
        {
            return $this->belongsTo('App\User', 'user_id', 'id');
        }
}
